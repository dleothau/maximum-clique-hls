#ifndef CLIQUE_H
#define CLIQUE_H

const int N = 256;
const int Ne = 8;

typedef char weight_t;
typedef short weight_sum_t;
typedef unsigned int word_t;

typedef struct
{
	word_t P[Ne][N][N/32];
	word_t R[Ne][N][N/32];
	short last[Ne];
} stacks_t;

typedef struct
{
	word_t P[N][N/32];
	word_t R[N][N/32];
	short last = -1;
} reserve_t;

#endif

