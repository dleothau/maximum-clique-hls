#include "krnl.h"

#define Nkernel 8

#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <sys/time.h>
#include <mutex>

#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1

#include <CL/cl2.hpp>

#define OCL_CHECK(error, call)                                                                   \
    call;                                                                                        \
    if (error != CL_SUCCESS) {                                                                   \
        printf("%s:%d Error calling " #call ", error code is: %d\n", __FILE__, __LINE__, error); \
        exit(EXIT_FAILURE);                                                                      \
    }

int order[N], order_inv[N];

void preprocess(const word_t mat[N][N/32],
		const weight_t weights[N],
		word_t M[N][N/32],
		weight_t w[N],
		const int size)
{
	int ordered[N];
	int neighbors_w[N];

	for (int i = 0; i<N; i++)
	{
	  order[i] = 0;
	  order_inv[i] = 0;
	  ordered[i] = 0;
	}

	int p = 0;
	for (int i = 0; i<size; i++)
	{
		neighbors_w[i] = 0;
		for (int j = 0; j<size; j++)
			if (mat[i][j/32] & (((word_t)1)<<(j%32)))
				neighbors_w[i] += (int)weights[j];
	}

	for (int cnt = 0; cnt < size; cnt++)
	{
		int min_w = INT_MAX;
		int max_w = -1;

		for (int u = size - 1; u >= 0; u--)
			if ((!ordered[u]) && (((int)weights[u]) < min_w))
				min_w = (int) weights[u];

		for (int u = size - 1; u >= 0; u--)
		{
			if (ordered[u] || ((int) weights[u] > min_w))
			{
				continue;
			}
			else if (neighbors_w[u] > max_w)
			{
				max_w = neighbors_w[u];
				p = u;
			}
		}

		order[cnt] = p;
		order_inv[p] = cnt;
		ordered[p] = 1;
		for (int j = 0; j<size; j++)
			if ((!ordered[j]) && (mat[p][j/32] & (((word_t)1)<<(j%32))))
				neighbors_w[j] -= (int) weights[p];
	}

	for (int i = 0; i<N; i++)
		for (int j = 0; j< N/32; j++)
			M[i][j] = (word_t)0;

	for (int i = 0; i<N; i++)
		for (int j = 0; j<N; j++)
			if (mat[i][j/32] & (((word_t)1<<(j%32))))
				M[order_inv[i]][order_inv[j]/32] |= (((word_t)1)<<(order_inv[j]%32));

	for (int i = 0; i<size; i++)
		w[order_inv[i]] = weights[i];
}

void postprocess(const word_t C[N/32],
		word_t C2[N/32])
{
	for (int i = 0; i<N/32; i++)
		C2[i] = (word_t)0;
	for (int i = 0; i<N; i++)
		if (C[i/32] & ((word_t)1<<(i%32)))
		{
			int tmp = order[i];
			C2[tmp/32] |= ((word_t)1<<(tmp%32));
		}
}


word_t mat[N][N/32], mat2[N][N/32];
weight_t w[N], w2[N];
weight_sum_t res_w = 0;
weight_sum_t prun[N];
word_t clique[N/32], C[N/32];

int n;
int next_vertex = 1;
int active_kernel = Nkernel;

struct timeval stop, start;


int map[Nkernel][N] = {0};

std::mutex mutex;
std::mutex sem_end;
typedef struct
{
	int kernel_num;
	int vertex;

	cl::Kernel* krnl;
	cl::Event* event;

	cl::Buffer* buffer_mat;
	cl::Buffer* buffer_w;
	cl::Buffer* buffer_prun;
	cl::Buffer* buffer_w_res;
	cl::Buffer* buffer_result;


	word_t** ptr_mat;
	weight_t** ptr_w;
	weight_sum_t** ptr_prun;
	word_t** ptr_result;
	weight_sum_t** ptr_res_w;

	cl::CommandQueue* q;
	cl::CommandQueue* q2;


} callback_arg_t;

void CL_CALLBACK kernel_end_callback(cl_event evt, cl_int event_status, void* arg)
{
	callback_arg_t* typed_arg = (callback_arg_t*)arg;
	int kernel_num = typed_arg->kernel_num;


	mutex.lock();

	int i = next_vertex;

	typed_arg->q2->enqueueMigrateMemObjects({typed_arg->buffer_mat[kernel_num], typed_arg->buffer_w[kernel_num],
    	typed_arg->buffer_result[kernel_num], typed_arg->buffer_w_res[kernel_num],
		typed_arg->buffer_prun[kernel_num]},CL_MIGRATE_MEM_OBJECT_HOST);
	typed_arg->q2->finish();
    prun[typed_arg->vertex] = typed_arg->ptr_res_w[kernel_num][0];

    std::cout << typed_arg->vertex << "-" << typed_arg->ptr_res_w[kernel_num][0] << std::endl;

    if (typed_arg->ptr_res_w[kernel_num][0] > res_w)
    {
    	res_w = typed_arg->ptr_res_w[kernel_num][0];
    	for (int j = 0; j<N/32; j++)
    		clique[j] = 0;
    	for (int j = 0; j<=N; j++)
    		if (typed_arg->ptr_result[kernel_num][j/32] & ((word_t)1<<(j%32)))
    			clique[map[kernel_num][j]/32] |= ((word_t)1<<(map[kernel_num][j]%32));
    }

	if (next_vertex < n)
	{
		int index = 0;

    	for (int j = 0; j<i; j++)
    		if (mat2[i][j/32] & (((word_t)1)<<(j%32)))
    		{
    			map[kernel_num][index] = j;
    			index++;
    		}

    	for (int j = 0; j<N; j++)
    	{
    		for (int k = 0; k<N/32; k++)
    		{
    			typed_arg->ptr_mat[kernel_num][(N/32)*j+k] = (word_t)0;
    		}
    	}

    	for (int j = 0; j<index; j++)
    	{
    		for (int k = 0; k<index; k++)
    			if (mat2[map[kernel_num][j]][map[kernel_num][k]/32] & (((word_t)1)<<(map[kernel_num][k]%32)))
    			{
    				typed_arg->ptr_mat[kernel_num][(N/32) * j + k/32] |= ((word_t)1)<<(k%32);
    			}
    	}

    	for (int j = 0; j<index; j++)
    	{
    		typed_arg->ptr_w[kernel_num][j] = w2[map[kernel_num][j]];
    		typed_arg->ptr_prun[kernel_num][j] = prun[map[kernel_num][j]];
    	}
    	typed_arg->ptr_w[kernel_num][index] = w2[i];
    	map[kernel_num][index] = i;

    	typed_arg->q2->enqueueMigrateMemObjects({typed_arg->buffer_mat[kernel_num],
    		typed_arg->buffer_w[kernel_num], typed_arg->buffer_prun[kernel_num]},0);
    	typed_arg->q2->finish();


        int narg=0;
        typed_arg->krnl[kernel_num].setArg(narg++,typed_arg->buffer_mat[kernel_num]);
        typed_arg->krnl[kernel_num].setArg(narg++,typed_arg->buffer_w[kernel_num]);
        typed_arg->krnl[kernel_num].setArg(narg++,typed_arg->buffer_result[kernel_num]);
        typed_arg->krnl[kernel_num].setArg(narg++,index);
        typed_arg->krnl[kernel_num].setArg(narg++, res_w);
        typed_arg->krnl[kernel_num].setArg(narg++, typed_arg->buffer_w_res[kernel_num]);
        typed_arg->krnl[kernel_num].setArg(narg++, typed_arg->buffer_prun[kernel_num]);


        typed_arg->vertex = next_vertex;
        next_vertex++;
        typed_arg->event[kernel_num] = cl::Event();

        typed_arg->q->enqueueTask(typed_arg->krnl[kernel_num], nullptr, &(typed_arg->event[kernel_num]));
        typed_arg->event[kernel_num].setCallback(CL_COMPLETE, &kernel_end_callback, arg);



	}
	else
	{
		active_kernel--;
		if (active_kernel == 0)
		{
		    sem_end.unlock();
		}
	}
	mutex.unlock();

}


int main(int argc, char* argv[]) {
	cl::Kernel krnl[Nkernel];
		cl::Event event[Nkernel];

		cl::Buffer buffer_mat[Nkernel];
		cl::Buffer buffer_w[Nkernel];
		cl::Buffer buffer_prun[Nkernel];
		cl::Buffer buffer_w_res[Nkernel];
		cl::Buffer buffer_result[Nkernel];


		word_t* ptr_mat[Nkernel];
		weight_t* ptr_w[Nkernel];
		weight_sum_t* ptr_prun[Nkernel];
		word_t* ptr_result[Nkernel];
		weight_sum_t* ptr_res_w[Nkernel];

		cl::CommandQueue q, q2;


    //TARGET_DEVICE macro needs to be passed from gcc command line
    if(argc != 4) {
		std::cout << "Usage: " << argv[0] <<" <xclbin> <graph file> <output file>" << std::endl;
		return EXIT_FAILURE;
	}
    std::string graph_file;
    std::string output_file;
    graph_file = argv[2];
    output_file = argv[3];


    std::string xclbinFilename = argv[1];
    
    std::vector<cl::Device> devices;
    cl::Device device;
    cl_int err;
    cl::Context context;
    cl::Program program;
    std::vector<cl::Platform> platforms;
    bool found_device = false;

    cl::Platform::get(&platforms);
    for(size_t i = 0; (i < platforms.size() ) & (found_device == false) ;i++){
        cl::Platform platform = platforms[i];
        std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
        if ( platformName == "Xilinx"){
            devices.clear();
            platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);
	    if (devices.size()){
		    device = devices[0];
		    found_device = true;
		    break;
	    }
        }
    }
    if (found_device == false){
       std::cout << "Error: Unable to find Target Device " 
           << device.getInfo<CL_DEVICE_NAME>() << std::endl;
       return EXIT_FAILURE; 
    }

    // Creating Context and Command Queue for selected device
    OCL_CHECK(err, context = cl::Context(device, NULL, NULL, NULL, &err));
    OCL_CHECK(err, q = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err));

    std::cout << "INFO: Reading " << xclbinFilename << std::endl;
    FILE* fp;
    if ((fp = fopen(xclbinFilename.c_str(), "r")) == nullptr) {
        printf("ERROR: %s xclbin not available please build\n", xclbinFilename.c_str());
        exit(EXIT_FAILURE);
    }
    // Load xclbin 
    std::cout << "Loading: '" << xclbinFilename << "'\n";
    std::ifstream bin_file(xclbinFilename, std::ifstream::binary);
    bin_file.seekg (0, bin_file.end);
    unsigned nb = bin_file.tellg();
    bin_file.seekg (0, bin_file.beg);
    char *buf = new char [nb];
    bin_file.read(buf, nb);
    
    // Creating Program from Binary File
    cl::Program::Binaries bins;
    bins.push_back({buf,nb});
    devices.resize(1);
    OCL_CHECK(err, program = cl::Program(context, devices, bins, NULL, &err));
    
    // This call will get the kernel object from program. A kernel is an 
    // OpenCL function that is executed on the FPGA. 
    for (int i = 0; i<Nkernel; i++)
    	OCL_CHECK(err, krnl[i] = cl::Kernel(program,"find_clique_interlace", &err));




	gettimeofday(&start, nullptr);

	n = 0;

	for (int i = 0; i<N; i++)
	{
		prun[i] = (weight_sum_t)0;
		w[i] = (weight_t)0;
		for (int j = 0; j<N/32; j++)
			mat[i][j] = (word_t)0;
	}

	fp = fopen(graph_file.c_str(), "r");
	FILE* fp_res = fopen(output_file.c_str(), "w");

	if (!fp || !fp_res)
	{
		if(!fp)
			std::cout << "Cannot open file " << graph_file << "." << std::endl;
		if(!fp_res)
			std::cout << "Cannot open file " << output_file << "." << std::endl;
		return EXIT_FAILURE;
	}

    char* line = NULL;
    size_t len;
    char* p;

    while ( getline(&line, &len, fp) != -1 )
    {
    	if (line[0] == 'p')
    	{
    		p = &(line[0]);
    		for (int i = 0; i<2; i++)
    		{
    			while (*p != ' ')
    				p++;
    			p++;
    		}
    		n = atoi(p);
    		if (n > N)
    		{
    			std::cout << "Error: input graph should not have more than " << N << " vertices." << std::endl;
    			exit(0);
    		}
    	}
    	if (line[0] == 'n')
    	{
    		int node, weight;
    		p = &(line[0]);

    		while (*p != ' ')
    			p++;
    		p++;
    		node = atoi(p) - 1;
    		while (*p != ' ')
    			p++;
    		weight = atoi(p);
    		w[node] = (weight_t)weight;
    	}
    	if (line[0] == 'e')
    	{
    		int node1, node2;
    		p = &(line[0]);
    		while (*p != ' ')
    			p++;
			p++;
			node1 = atoi(p) - 1;
			while (*p != ' ')
				p++;
			node2 = atoi(p) - 1;
			mat[node1][node2/32] |= (((word_t)1)<<(node2%32));
			mat[node2][node1/32] |= (((word_t)1)<<(node1%32));
    	}
    }

	fclose(fp);
	if (line)
		free(line);


	for (int i = 0; i<Nkernel; i++)
	{
		buffer_mat[i] = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(mat));
		buffer_w[i] = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(w));
		buffer_prun[i] = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(prun));
		buffer_w_res[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(weight_sum_t));
		buffer_result[i] = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(clique));

		ptr_mat[i] = (word_t*) q.enqueueMapBuffer (buffer_mat[i] , CL_TRUE , CL_MAP_WRITE , 0, sizeof(mat));
		ptr_w[i] = (weight_t*) q.enqueueMapBuffer (buffer_w[i] , CL_TRUE , CL_MAP_WRITE , 0, sizeof(w));
		ptr_prun[i] = (weight_sum_t*) q.enqueueMapBuffer (buffer_prun[i] , CL_TRUE , CL_MAP_WRITE , 0, sizeof(prun));
		ptr_result[i] = (word_t*) q.enqueueMapBuffer (buffer_result[i] , CL_TRUE , CL_MAP_READ , 0, sizeof(clique));
		ptr_res_w[i] = (weight_sum_t*) q.enqueueMapBuffer(buffer_w_res[i], CL_TRUE, CL_MAP_READ, 0, sizeof(weight_sum_t));
	}
    preprocess(mat, w, mat2, w2, n);




    res_w = w2[0];
    for (int i = 0; i<N; i++)
    {
    	weight_sum_t wG = 0;
    	for (int j = 0; j<=i; j++)
    		wG += w2[j];
    	prun[i] = wG;
    }
    prun[0] = w2[0];
    for (int i = 0; i<N/32; i++)
    	clique[i] = (word_t)0;
    clique[0] = (word_t)1;

    OCL_CHECK(err, q2 = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err));


    mutex.lock();
    sem_end.lock();
    for (int i = 1; i<Nkernel+1; i++)
    {
    	callback_arg_t* arg = (callback_arg_t*)malloc(sizeof(callback_arg_t));
    	int kernel_num = arg->kernel_num = i-1;

    	arg->krnl = &(krnl[0]);
    	arg->event = &(event[0]);
    	arg->buffer_mat = &(buffer_mat[0]);
    	arg->buffer_w = &(buffer_w[0]);
    	arg->buffer_prun = &(buffer_prun[0]);
    	arg->buffer_result = &(buffer_result[0]);
    	arg->buffer_w_res = &(buffer_w_res[0]);
    	arg->ptr_mat = &(ptr_mat[0]);
    	arg->ptr_w = &(ptr_w[0]);
    	arg->ptr_prun = &(ptr_prun[0]);
    	arg->ptr_result = &(ptr_result[0]);
    	arg->ptr_res_w = &(ptr_res_w[0]);

    	arg->q = &q;
    	arg->q2 = &q2;

		int index = 0;

    	for (int j = 0; j<i; j++)
    		if (mat2[i][j/32] & (((word_t)1)<<(j%32)))
    		{
    			map[kernel_num][index] = j;
    			index++;
    		}
    	for (int j = 0; j<N; j++)
    	{
    		for (int k = 0; k<N/32; k++)
    		{
    			ptr_mat[kernel_num][(N/32)*j+k] = (word_t)0;
    		}
    	}

    	for (int j = 0; j<index; j++)
    	{
    		for (int k = 0; k<index; k++)
    			if (mat2[map[kernel_num][j]][map[kernel_num][k]/32] & (((word_t)1)<<(map[kernel_num][k]%32)))
    			{
    				ptr_mat[kernel_num][(N/32) * j + k/32] |= ((word_t)1)<<(k%32);
    			}
    	}

    	for (int j = 0; j<index; j++)
    	{
    		ptr_w[kernel_num][j] = w2[map[kernel_num][j]];
    		ptr_prun[kernel_num][j] = prun[map[kernel_num][j]];
    	}
    	ptr_w[kernel_num][index] = w2[i];
    	map[kernel_num][index] = i;

    	q2.enqueueMigrateMemObjects({buffer_mat[kernel_num], buffer_w[kernel_num], buffer_prun[kernel_num]},0);
        q2.finish();



        int narg=0;
        krnl[kernel_num].setArg(narg++,buffer_mat[kernel_num]);
        krnl[kernel_num].setArg(narg++,buffer_w[kernel_num]);
        krnl[kernel_num].setArg(narg++,buffer_result[kernel_num]);
        krnl[kernel_num].setArg(narg++,i);
        krnl[kernel_num].setArg(narg++, res_w);
        krnl[kernel_num].setArg(narg++, buffer_w_res[kernel_num]);
        krnl[kernel_num].setArg(narg++, buffer_prun[kernel_num]);

        arg->vertex = next_vertex;
        next_vertex++;

        event[kernel_num] = cl::Event();

        q.enqueueTask(krnl[kernel_num], nullptr, &(event[kernel_num]));
        event[kernel_num].setCallback(CL_COMPLETE, &kernel_end_callback, arg);

    }
    mutex.unlock();

    sem_end.lock();

    q.finish();
    q2.finish();


    for (int i = 0; i<Nkernel; i++)
    {
    	OCL_CHECK(err, err = q2.enqueueUnmapMemObject(buffer_mat[i] , ptr_mat[i]));
    	OCL_CHECK(err, err = q2.enqueueUnmapMemObject(buffer_w[i] , ptr_w[i]));
    	OCL_CHECK(err, err = q2.enqueueUnmapMemObject(buffer_result[i] , ptr_result[i]));
    	OCL_CHECK(err, err = q2.enqueueUnmapMemObject(buffer_prun[i] , ptr_prun[i]));
    	OCL_CHECK(err, err = q2.enqueueUnmapMemObject(buffer_w_res[i], ptr_res_w[i]));
    }
    OCL_CHECK(err, err = q2.finish());
    q2.finish();

    postprocess(clique, C);

    std::cout << "clique :" << std::endl;
    for (int i = 0; i<n; i++)
    	if (C[i/32] & ((word_t)1<<(i%32)))
    		std::cout << i+1 << "-";

    std::cout << std::endl << "weight: " << res_w << std::endl;


	gettimeofday(&stop, nullptr);
    std::cout << "took " << (stop.tv_sec - start.tv_sec) * 1000000 + (stop.tv_usec - start.tv_usec) << "us" << std::endl;



    return 0;
}

