#include "krnl.h"

extern "C" {


weight_sum_t weight(word_t U[N/32], weight_t w[N])
{
	// compute weight of U
	weight_sum_t res = (weight_sum_t)0;
computeWeightLoop:
	for (int i = 0; i<N/32; i++)
	{
		word_t tmp = U[i];
		innerWeightLoop:for (int j = 0; j<32; j++)
		{
			weight_sum_t cond = (tmp & (((word_t)1)<<j)) ? ~((weight_sum_t)0) : (weight_sum_t)0;
			res	+= ((weight_sum_t)w[32 * i + j]) & cond;
		}
	}
	return res;
}

word_t notEmpty(word_t P[N/32])
{
#pragma HLS INLINE
	// result is not zero iff P is not empty.
	word_t res = (word_t)0;
	notEmptyLoop:for (int i = 0; i<N/32; i++)
	{
#pragma HLS unroll
		res |= P[i];
	}
	return res;
}

int pickElt(word_t P[N/32], short* delta_last)
{
#pragma HLS INLINE
// pick an element of P, *isElt is set to 1 if there is a such element, else it is set to 0
	for (int i = N/32 -1; i>=0; i--)
	{
#pragma HLS unroll
		word_t tmp = P[i];
		for (int j = 31; j>=0; j--)
		{
#pragma HLS unroll
			if (tmp & (((word_t)1)<<j))
			{
				*delta_last = 1;
				return 32 * i + j;
			}
		}
	}
	*delta_last = -1;
	return 0;
}




void tomita_entrelacement_interlace(word_t mat[N][N/32],
		weight_t w[N], int n, weight_sum_t maxi_w, word_t clique[N/32],
		weight_sum_t* res_w, weight_sum_t prun[N], weight_t wn)
{
// Find the biggest clique in subgraph of mat with vertices <= n
	reserve_t reserve;
	stacks_t stack;
	weight_sum_t wmax[Ne];
	word_t clique_res[Ne][N/32];


#pragma HLS array_partition variable=clique_res factor=Ne cyclic dim=1
#pragma HLS array_partition variable=clique_res factor=N/64 cyclic dim=2
#pragma HLS array_partition variable=wmax factor=Ne cyclic

	for (int i = 0; i<Ne; i++)
	{
		wmax[i] = maxi_w;
		for (int j = 0; j<N/32; j++)
			clique_res[i][j] = 0;
	}

#pragma HLS array_partition variable=reserve.P dim=1 factor=2 cyclic
#pragma HLS array_partition variable=reserve.R dim=1 factor=2 cyclic
#pragma HLS array_partition variable=reserve.P dim=2 factor=2 cyclic
#pragma HLS array_partition variable=reserve.R dim=2 factor=2 cyclic


#pragma HLS array_partition variable=stack.P dim=1 factor=Ne cyclic
#pragma HLS array_partition variable=stack.P dim=2 factor=2 cyclic
#pragma HLS array_partition variable=stack.P dim=3 factor=N/32 cyclic
#pragma HLS array_partition variable=stack.R dim=1 factor=Ne cyclic
#pragma HLS array_partition variable=stack.R dim=2 factor=2 cyclic
#pragma HLS array_partition variable=stack.R dim=3 factor=N/32 cyclic
#pragma HLS array_partition variable=stack.last factor=Ne cyclic


	short last = -1;
reserveInitLoop:
	for (int i = N-1; i>=0; i--)
	{
#pragma HLS PIPELINE OFF
		if (i<n)
		{

			weight_sum_t tmp = prun[i] + wn - maxi_w;
			if (tmp > 0)
			{
				last++;
				for (int j = 0; j<N/32; j++)
				{
#pragma HLS unroll
					reserve.P[last][j] = mat[i][j];
					reserve.R[last][j] = (word_t)0;
				}
				reserve.R[last][i/32] = (word_t)1<<(i%32);
				reserve.R[last][n/32] |= (word_t)1<<(n%32);
				for (int j = 0; j<N; j++)
				{
#pragma HLS unroll
					if (i<j)
					{
						reserve.P[last][j/32] &= ~((word_t)1<<(j%32));
					}
				}
			}
		}
	}
	reserve.last = last;

	for (int i = 0; i<Ne; i++)
	{
#pragma HLS unroll
		stack.last[i] = -1;
	}

	char not_finished = Ne;
	char finished[Ne] = {0};


#pragma HLS array_partition variable=finished complete

	int cond[Ne];
#pragma HLS array_partition variable=cond complete
	for (int i = 0; i<Ne; i++)
		cond[i]= 1;



	char round = 0;
mainComputingLoop:
	while (cond[0])
	{
#pragma HLS pipeline II=1
#pragma HLS DEPENDENCE true variable=stack distance=Ne
#pragma HLS DEPENDENCE true variable=finished distance=Ne
#pragma HLS DEPENDENCE true variable=reserve distance=2


		word_t P[N/32], R[N/32];
		//weight_sum_t wmax_tmp = wmax[round];

#pragma HLS array_partition variable=P complete dim=0
#pragma HLS array_partition variable=R complete dim=0

		for (int i = 0; i<Ne-1; i++)
		{
#pragma HLS unroll
			cond[i] = cond[i+1];
		}
		cond[Ne-1] = ((reserve.last >= 0) || not_finished);
		weight_sum_t w_round = wmax[round];

		if (stack.last[round] >= 0)
		{
			short last = stack.last[round];
			for (int i = 0; i<N/32; i++)
			{
	#pragma HLS unroll
				P[i] = stack.P[round][last][i];
				R[i] = stack.R[round][last][i];
			}

			short delta_last;
			int u = pickElt(P, &delta_last);
			short next_last = last + delta_last;

			word_t notEmptyP = notEmpty(P);

			char wR_minus_wmax;
			char wRP_minus_wmax;
			char wRu_minus_wmax;

			weight_sum_t wR = weight(R, w);
			weight_sum_t wP = weight(P, w);

			wR_minus_wmax = wR - w_round >= 0;
			wRP_minus_wmax = wR - w_round + wP >= 0;
			wRu_minus_wmax = wR - w_round + prun[u] >= 0;

			stack.P[round][last][u/32] = (P[u/32]) & (~(((word_t)1)<<(u%32)));

			for (int i = 0; i<N/32; i++)
			{
				word_t tmpR = 0;
				if (i==u/32)
					tmpR = ((word_t)1)<<(u%32);
				stack.P[round][last+1][i] = P[i] & mat[u][i];
				stack.R[round][last+1][i] = tmpR | R[i];
			}

			if (wRP_minus_wmax)
			{
				if (wRu_minus_wmax)
				{
					if (wR_minus_wmax)
					{
						for (int i = 0; i<N/32; i++)
						{
#pragma HLS unroll
							clique_res[round][i] = R[i];
						}
						wmax[round] = wR;
					}
					stack.last[round] = next_last;
				}
			}
			else
			{
				stack.last[round] = last - 1;
			}

		}
		else
		{

			int ind = reserve.last;
			if (ind >= 0)
			{
				reserve.last = ind - 1;
				for (int i = 0; i<N/32; i++)
				{
					stack.P[round][0][i] = reserve.P[ind][i];
					stack.R[round][0][i] = reserve.R[ind][i];
				}
				stack.last[round] = 0;
			}
			else if (!finished[round])
			{

				finished[round]=1;
				not_finished--;
			}
		}


		round = (round+1)%Ne;
	}

	int ind_max = 0;
	int ind_w = 0;
	for (int i = 0; i<Ne; i++)
	{
#pragma HLS pipeline off
		if (wmax[i] > ind_w)
		{
			ind_max = i;
			ind_w = wmax[i];
		}
	}



	for (int i = 0; i<N/32; i++)
	{
#pragma HLS unroll
		clique[i] = clique_res[ind_max][i];
	}
	*res_w = ind_w;


}


void find_clique_interlace(const word_t mat[N * N/32],
		const weight_t w[N],
		word_t* clique,
		int ref_vertex,
		weight_sum_t max_w,
		weight_sum_t* res_w,
		weight_sum_t prun[N])
{


	word_t M[N][N/32] = {0};
	weight_t weights[N];
	weight_sum_t P[N] = {0};


#pragma HLS array_partition variable=M dim=1 factor=4 cyclic
#pragma HLS array_partition variable=M dim=2 factor=8 cyclic

loopInitM:
	for (int i = 0; i<N; i++)
		for (int j = 0; j<N/32; j++)
			M[i][j] = mat[i * (N/32) + j];


loopInitWandPrun:
	for (int i = 0; i<N; i++)
	{
#pragma HLS unroll
		weights[i] = w[i];
		P[i] = prun[i];
	}


	tomita_entrelacement_interlace(M, weights, ref_vertex, max_w, clique, res_w,
				P, w[ref_vertex]);


}

}

